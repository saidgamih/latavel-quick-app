<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

    <link rel="stylesheet" href="https://cdn.usebootstrap.com/bootstrap/4.6.0/css/bootstrap.min.css">
</head>

<body class="antialiased">

    <div style="width: 500px; margin: 50px auto;">
        @if(session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <form action="{{ route('store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" id="title" class="form-control @error('title') is-invalid @enderror">
                @error('title')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <textarea name="body" id="body" class="form-control @error('body') is-invalid @enderror" rows="10"></textarea>
                @error('body')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="text-center">
                <button class="btn btn-primary">Submit</button>
            </div>
        </form>

        <div class="mt-4">
            <div class="list-group">
                @forelse($todos as $todo)
                  <a href="#" class="list-group-item list-group-item-action">
                    <div class="d-flex w-100 justify-content-between">
                      <h5 class="mb-1">{{ $todo->title }}</h5>
                      <small>{{ $todo->created_at->diffForHumans() }}</small>
                    </div>
                    <p class="mb-1">{{ $todo->body }}</p>
                  </a>
                @empty
                  <div class="text-center text-muted">No todos found</div>
                @endforelse
            </div>
        </div>
    </div>

</body>

</html>