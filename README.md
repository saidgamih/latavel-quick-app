1. Install dependencies
> `composer install`
---
2. Rename `.env.example` to `.env`
3. Edit database credentials in `.env` file
`DB_DATABASE=your_db_name`
`DB_USERNAME=your_db_username`
`DB_PASSWORD=password` 
---
4. Generate an application key
> `php artisan key:generate`
---
5. Run the migrations
> `php artisan migrate`
---
6. Run the local server
> `php artisan serve`