<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function index()
    {
        $todos = Todo::latest()->get();
        return view('index', compact('todos'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title' => 'required|min:3|max:250',
            'body' => 'required|min:4'
        ]);

        Todo::create($data);

        return redirect(route('index'))->with('success', 'Todo added successfuly !');
    }
}
